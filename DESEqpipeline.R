if (!requireNamespace("BiocManager", quietly = TRUE)) {
  install.packages("BiocManager")
  BiocManager::install("DESeq2")
  install.packages(c('R.utils','data.table','RCpp','curl'))
  BiocManager::install("GEOquery")
}

require(DESeq2)
require(data.table)

runDESEQ <- function(rc, coldata, outputFile, contrast) {
  colnames(coldata) <- c('group','sample')
  coldata <- data.frame(coldata)
  rownames(coldata) <- coldata$sample
  coldata$group <- as.factor(coldata$group)
  
  cts <- subset(rc, select=as.character(coldata$sample))
  rownames(cts)<-rc$V1
  
  stopifnot(all(rownames(coldata) %in% colnames(cts)))
  ## [1] TRUE
  stopifnot(all(rownames(coldata) == colnames(cts)))
  ## [1] FALSE
  stopifnot(all(rownames(coldata) == colnames(cts)))
  ## [1] TRUE
  
  keep <- rowSums(cts) > 0
  cts <- cts[keep,]
  rownames(cts)<-rc$V1[keep]
  
  cts <- as.matrix(cts)
  rownames(cts)<-rc$V1[keep]
  dds <- DESeqDataSetFromMatrix(countData = cts,
                                colData = coldata,
                                design= ~ group)
  dds <- DESeq(dds)
  
  res <- results(dds, contrast=c("group", make.names(contrast$condition), make.names(contrast$control)))
  res <- res[!is.na(res$padj),]
  print(head(res))
  print(paste("saving file to ", outputFile))
  write.csv(res, outputFile)
}

selectSamples <- function(rc, conditionName, controlName){
  conditionIdx <- sort(grep(conditionName, colnames(rc)), decreasing = T)
  controlIdx <- sort(grep(controlName, colnames(rc), fixed = T), decreasing = T)
  return(list(conditionIdx = conditionIdx, controlIdx = controlIdx, cols = colnames(rc)[c(conditionIdx, controlIdx)]))
}


rc = fread("https://ftp.ncbi.nlm.nih.gov/geo/series/GSE147nnn/GSE147507/suppl/GSE147507_RawReadCounts_Human.tsv.gz")
head(rc)
#testing
all(selectSamples(rc, 'Series1_NHBE_SARS-CoV-2', 'Series1_NHBE_Mock')$cols == colnames(rc)[7:2] )
all(selectSamples(rc, 'Series2_A549_SARS-CoV-2', 'Series2_A549_Mock')$cols == colnames(rc)[13:8] )
all(selectSamples(rc, 'Series3_A549_RSV', 'Series3_A549_Mock')$cols == colnames(rc)[17:14] )
all(selectSamples(rc, 'Series4_A549_IAV', 'Series4_A549_Mock')$cols == colnames(rc)[21:18] )
all(selectSamples(rc, 'Series15_COVID19Lung', 'Series15_HealthyLungBiopsy')$cols == colnames(rc)[70:67] )

buildDiffExpForAllContrasts <- function(rc, listOfContrasts, gseId) {
  for(contrast in listOfContrasts) {
    print(paste(rep('-',50),collapse=""))
    print (contrast$condition)
    print (contrast$control)
    samples <- selectSamples(rc, contrast$condition, contrast$control)
    
    coldata<-cbind(
      c(rep(make.names(contrast$condition), length(samples$conditionIdx)), rep(make.names(contrast$control), length(samples$controlIdx))), 
      samples$cols
    )
    print(coldata)
    runDESEQ(rc, coldata, paste(gseId, "_", contrast$condition, '_VS_',contrast$control,".deseq2", sep=""), contrast)
  }
}


# GSE147507
# https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSE147507&format=file&file=GSE147507%5FRawReadCounts%2Etsv%2Egz
rc = fread("https://ftp.ncbi.nlm.nih.gov/geo/series/GSE147nnn/GSE147507/suppl/GSE147507_RawReadCounts_Human.tsv.gz")
print(dim(rc))
buildDiffExpForAllContrasts(rc, 
                            list(
                              list(condition='Series1_NHBE_SARS-CoV-2',control='Series1_NHBE_Mock'),
                              list(condition='Series2_A549_SARS-CoV-2',control='Series2_A549_Mock'),
                              list(condition='Series3_A549_RSV',control='Series3_A549_Mock'),
                              list(condition='Series4_A549_IAV',control='Series4_A549_Mock'),
                              list(condition='Series5_A549_SARS-CoV-2',control='Series5_A549_Mock'),
                              list(condition='Series6_A549-ACE2_SARS-CoV-2',control='Series6_A549-ACE2_Mock'),
                              list(condition='Series7_Calu3_SARS-CoV-2',control='Series7_Calu3_Mock'),
                              list(condition='Series8_A549_RSV',control='Series8_A549_Mock'),
                              list(condition='Series8_A549_HPIV3',control='Series8_A549_Mock'),
                              list(condition='Series9_NHBE_IAV',control='Series9_NHBE_Mock'),
                              list(condition='Series9_NHBE_IAVdNS1',control='Series9_NHBE_Mock'),
                              list(condition='Series9_NHBE_IFNB_4h',control='Series9_NHBE_Mock'),
                              list(condition='Series9_NHBE_IFNB_6h',control='Series9_NHBE_Mock'),
                              list(condition='Series9_NHBE_IFNB_12h',control='Series9_NHBE_Mock'),
                              list(condition='Series15_COVID19Lung',control='Series15_HealthyLungBiopsy'),
                              list(condition='Series16_A549-ACE2_SARS-CoV-2_[0-9]',control='Series16_A549-ACE2_Mock'),
                              list(condition='Series16_A549-ACE2_SARS-CoV-2_Rux',control='Series16_A549-ACE2_Mock')
                            ), "GSE147507")


###GSE150316
rc_z<-fread("https://ftp.ncbi.nlm.nih.gov/geo/series/GSE150nnn/GSE150316/suppl/GSE150316_RawCounts_Final.txt.gz")
contrast<-list(condition='Covid19', control='Mock')
col_z<-c(selectSamples(rc_z, 'case1-lung', 'XXX')$cols,
         selectSamples(rc_z, 'case5-lung', 'XXX')$cols,
         selectSamples(rc_z, 'case7-lung', 'XXX')$cols,
         selectSamples(rc_z, 'case8-lung', 'XXX')$cols,
         selectSamples(rc_z, 'case9-lung', 'XXX')$cols,
         selectSamples(rc_z, 'caseC-lung', 'XXX')$cols,
         selectSamples(rc_z, 'caseD-lung', 'XXX')$cols,
         selectSamples(rc_z, 'case11-lung', 'NegControl')$cols
         )
coldata<-cbind(
  c(gsub('.*lung.*',contrast$condition, (gsub('.*NegControl.*',contrast$control, col_z)))),
  c(col_z)
)
runDESEQ(rc=rc_z, coldata = coldata, outputFile ="GSE150316.deseq", contrast = contrast)

# GSE160435
rc_y<-fread("https://ftp.ncbi.nlm.nih.gov/geo/series/GSE160nnn/GSE160435/suppl/GSE160435_count.csv.gz")
contrast<-list(condition='Covid19', control='Mock')
col_y<-selectSamples(rc_y, 'covid', 'mock')$cols
coldata<-cbind(
  c(gsub('.*covid.*',contrast$condition, (gsub('.*mock.*',contrast$control, col_y)))),
  c(col_y)
)
rc_y <- rc_y[!duplicated(rc_y$V1),]
runDESEQ(rc=rc_y, coldata = coldata, outputFile ="GSE160435.deseq", contrast = contrast)


###GSE148729
rc<-fread("https://ftp.ncbi.nlm.nih.gov/geo/series/GSE148nnn/GSE148729/suppl/GSE148729_Caco2_polyA_readcounts.txt.gz")
colnames(rc)
rc$V1<-rc$gene_id

buildDiffExpForAllContrasts(rc, 
                            list(
                              list(condition='Caco2_polyA-S2-24h',control='Caco2_polyA-mock-24h')
                            ), "GSE148729")


###GSE149312 # see https://science.sciencemag.org/content/suppl/2020/04/30/science.abc1669.DC1
# rc<-fread("~/Downloads/GSE149312_corona_intestine_exp1_ndata.csv.gz")
# dim(rc)
# 
# rc$V1<-unlist(lapply(strsplit(rc$V1, '__'), function(y) {y[[1]]}))
# buildDiffExpForAllContrasts(rc, 
#                             list(
#                               list(condition='1b24',control='1_'),
#                               list(condition='1b60',control='1_'),
#                               list(condition='6a24',control='6_'),
#                               list(condition='6a60',control='6_'),
#                               list(condition='6b24',control='6_'),
#                               list(condition='6b60',control='6_')
#                             ), "GSE149312")


#GSE151803 - Liver and Pancreas, there is Lung data but without control
rc<-fread("https://ftp.ncbi.nlm.nih.gov/geo/series/GSE151nnn/GSE151803/suppl/GSE151803_counts.txt.gz")
dim(rc)

origCols<-colnames(rc)

# where necessary, manually rename samples based on GEO data https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE151803
colnames(rc)<-c('V1', origCols[2:7], 
                 c('GSM4591156_L2_COVID-19_Lung_51L',
                   'GSM4591157_L3_COVID-19_Lung_56L',
                   'GSM4591158_L4_COVID-19_Lung_58L'),
                c('GSM4591144_X45_1_Organoid_Donor_1_Liver_Mock_1',
                  'GSM4591145_X45_2_Organoid_Donor_1_Liver_Mock_2',
                  'GSM4591146_X45_3_Organoid_Donor_1_Liver_SARS-CoV-2_1',
                  'GSM4591147_X50_1_Organoid_Donor_1_Liver_Mock_R2_1',
                  'GSM4591148_X50_2_Organoid_Donor_1_Liver_Mock_R2_1',
                  'GSM4591149_X50_3_Organoid_Donor_1_Liver_SARS-CoV-2_R2_1',
                  'GSM4591150_X50_4_Organoid_Donor_1_Liver_SARS-CoV-2_R2_2',
                  'GSM4591151_X50_5_Organoid_Donor_2_Liver_Mock_R2_1',
                  'GSM4591152_X50_6_Organoid_Donor_2_Liver_Mock_R2_2',
                  'GSM4591153_X50_7_Organoid_Donor_2_Liver_SARS-CoV-2_R2_1',
                  'GSM4591154_X50_8_Organoid_Donor_2_Liver_SARS-CoV-2_R2_2',
                  'GSM4591155_X52_22_Organoid_Donor_1_Liver_SARS-CoV-2_2')
                 )
buildDiffExpForAllContrasts(rc, 
                            list(
                              list(condition='Pancreas.SARSCoV2',control='Pancreas.mock'),
                              list(condition='Organoid_Donor_1_Liver_SARS-CoV-2',control='Organoid_Donor_1_Liver_Mock'),
                              list(condition='Organoid_Donor_2_Liver_SARS-CoV-2',control='Organoid_Donor_2_Liver_Mock'),
                              list(condition='Organoid_Donor_1_Liver_SARS-CoV-2_R2',control='Organoid_Donor_1_Liver_Mock_R2'),
                              list(condition='Organoid_Donor_2_Liver_SARS-CoV-2_R2',control='Organoid_Donor_2_Liver_Mock_R2'),
                              list(condition='Liver_SARS-CoV-2',control='Liver_Mock')
                            ), "GSE149312")


#GSE152075 nasopharyngeal swabs from 430 individuals with SARS-CoV-2 and 54 negative controls
#

rc<-fread("https://ftp.ncbi.nlm.nih.gov/geo/series/GSE152nnn/GSE152075/suppl/GSE152075_raw_counts_GEO.txt.gz")
dim(rc)

gse <- getGEO("GSE152075")
title <- c("", make.names(gse$GSE152075_series_matrix.txt.gz$title))
gender <- c("", make.names(gse$GSE152075_series_matrix.txt.gz$characteristics_ch1.3))

colnames(rc)<-paste(gender, colnames(rc), sep="_")
colnames(rc)[1]<-'V1'

swabs <- list(
  list(condition='POS',control='NEG'),
  list(condition='gender..M_POS',control='gender..M_NEG'),
  list(condition='gender..F_POS',control='gender..F_NEG'),
  list(condition='gender..not.collected_POS',control='NEG')
)

buildDiffExpForAllContrasts(rc, swabs, "GSE152075_by_gender")

rc<-fread("https://ftp.ncbi.nlm.nih.gov/geo/series/GSE152nnn/GSE152075/suppl/GSE152075_raw_counts_GEO.txt.gz")
dim(rc)

gse <- getGEO("GSE152075")
title <- c("", make.names(gse$GSE152075_series_matrix.txt.gz$title))
gender <- c("", make.names(gse$GSE152075_series_matrix.txt.gz$characteristics_ch1.3))
age <- c("", make.names(gse$GSE152075_series_matrix.txt.gz$characteristics_ch1.2))
infection <- c("", make.names(gse$GSE152075_series_matrix.txt.gz$characteristics_ch1))

ageGroups <- c("", unlist(lapply(strsplit(make.names(gse$GSE152075_series_matrix.txt.gz$characteristics_ch1.2), '..', fixed = T), function(y) {
  
  if (!is.na(as.numeric(y[[2]])) && as.numeric(y[[2]])>=65) {
    return ("ge65")
  }
  if (!is.na(as.numeric(y[[2]])) && as.numeric(y[[2]])>=55 && as.numeric(y[[2]])<65 ) {
    return ("ge55lt65")
  }
  if (!is.na(as.numeric(y[[2]])) && as.numeric(y[[2]])>=45 && as.numeric(y[[2]])<55 ) {
    return ("ge45lt55")
  }
  if (!is.na(as.numeric(y[[2]])) && as.numeric(y[[2]])>=35 && as.numeric(y[[2]])<45 ) {
    return ("ge35lt45")
  }
  if (!is.na(as.numeric(y[[2]])) && as.numeric(y[[2]])>=25 && as.numeric(y[[2]])<35 ) {
    return ("ge25lt35")
  }
  if (!is.na(as.numeric(y[[2]])) && as.numeric(y[[2]])>=15 && as.numeric(y[[2]])<25 ) {
    return ("ge15lt25")
  }
  if (!is.na(as.numeric(y[[2]])) && as.numeric(y[[2]])<15 ) {
    return ("ge00lt15")
  }
  return(y[[2]])
})))

# to group by gender and age we inspect the number of samples by age groups first.
# sampleCounts <- as.data.frame(cbind(ageGroups, gender, infection))
# View(sampleCounts %>% group_by(ageGroups, gender, infection) %>% count(infection))


# revisit age groups such that we have at least 6 samples in positive group
ageGroups <- c("", unlist(lapply(strsplit(make.names(gse$GSE152075_series_matrix.txt.gz$characteristics_ch1.2), '..', fixed = T), function(y) {
  
  if (!is.na(as.numeric(y[[2]])) && as.numeric(y[[2]])>=55) {
    return ("ge55")
  }
  if (!is.na(as.numeric(y[[2]])) && as.numeric(y[[2]])>=35 && as.numeric(y[[2]])<55 ) {
    return ("ge35lt55")
  }
  if (!is.na(as.numeric(y[[2]])) && as.numeric(y[[2]])<35 ) {
    return ("ge00lt35")
  }
  return(y[[2]])
})))


swabs <- list()
i <- length(swabs) + 1
for(ageG in c('ge55',
              'ge35lt55',
              'ge00lt35'
)) {
  for (genderGroup in c('gender..M','gender..F'))  {
    swabs[[i]] <- list(condition=paste(ageG, genderGroup, 'POS', sep="_"), control=paste(ageG, genderGroup, 'NEG', sep="_"))
    i <- i + 1
  }
}
colnames(rc)<-paste(ageGroups, gender, colnames(rc), sep="_")
colnames(rc)[1]<-'V1'
buildDiffExpForAllContrasts(rc, swabs, "GSE152075_by_gender_and_age")



# GSE154769 5,6,7 days after infection

rc<-fread("https://ftp.ncbi.nlm.nih.gov/geo/series/GSE154nnn/GSE154769/suppl/GSE154769_longitudinal_raw_counts.txt.gz")

rc$V1<-rc$gene

buildDiffExpForAllContrasts(rc, 
                            list(
                              list(condition='coll_2',control='coll_1')
                            ), "GSE154769")

# GSE150392 -  https://ftp.ncbi.nlm.nih.gov/geo/series/GSE150nnn/GSE150392/suppl/GSE150392_Cov_Mock_Raw_COUNTS.csv.gz - 

# cell line: 02iCTR  - Human iPSC-cardiomyocytes (hiPSC-CMs)

rc<-fread("https://ftp.ncbi.nlm.nih.gov/geo/series/GSE150nnn/GSE150392/suppl/GSE150392_Cov_Mock_Raw_COUNTS.csv.gz")
dim(rc)

rc <- rc[grepl('^ENSG', rc$V1), ]
rc$V1<-unlist(lapply(strsplit(rc$V1, '_'), function(y) {y[[2]]}))
#exclude duplicates by gene symbol here - is there a better way of doing this?
rc <- rc[!duplicated(rc$V1),]


buildDiffExpForAllContrasts(rc, 
                            list(
                              list(condition='Cov',control='Mock')
                              ), "GSE150392")



# GSE153940 Vero E6 Cells

require(GEOquery)
gse<-getGEO("GSE153940")

gse$`GSE153940-GPL28836_series_matrix.txt.gz`$geo_accession

gsm1<-fread("https://ftp.ncbi.nlm.nih.gov/geo/samples/GSM4658nnn/GSM4658803/suppl/GSM4658803_1_NI_1_rsem.genes.results.txt.gz")
gsm1$expected_count

x<-NULL
rc<-NULL
genes<-NULL
for (gsm in c(gse$`GSE153940-GPL28836_series_matrix.txt.gz`$geo_accession, gse$`GSE153940-GPL28837_series_matrix.txt.gz`$geo_accession)) {
  gsmGEO <- getGEO(gsm)
  x <- fread(gsmGEO@header$supplementary_file_1)
  rc<-cbind(rc, x$expected_count)
  genes<-cbind(genes, x$gene_id)
}
rc<-data.table(ceiling(rc))
colnames(rc)<-c( make.names(gse$`GSE153940-GPL28836_series_matrix.txt.gz`$title) , make.names(gse$`GSE153940-GPL28837_series_matrix.txt.gz`$title))
rc$V1<-x$gene_id
rc$V1<-unlist(lapply(strsplit(rc$V1, '_'), function(y) {ifelse(length(y)>1, y[[2]], NA)}))
rc<-rc[!is.na(rc$V1),]    
rc <- rc[!duplicated(rc$V1),]
buildDiffExpForAllContrasts(rc, 
                            list(
                              list(condition='SARS.CoV.2',control='mock')
                            ), "GSE153940_VEROE6")


#  GSE154613

# Status	Public on Jul 17, 2020
# Title	Modulating the transcriptional landscape of SARS-CoV-2 as an effective method for developing antiviral compounds

require(GEOquery)
gse<-getGEO("GSE154613")
gse$GSE154613_series_matrix.txt.gz$characteristics_ch1.1

gse$GSE154613_series_matrix.txt.gz$geo_accession

x<-NULL
rc<-NULL
genes<-NULL
titles<-NULL
for(gsm in gse$GSE154613_series_matrix.txt.gz$geo_accession) {
  gsmGEO <- getGEO(gsm)
  x <- fread(gsmGEO@header$supplementary_file_1)
  rc<-cbind(rc, x$V2)
  genes<-cbind(genes, x$V1)
  titles <- c(titles, gsmGEO@header$title)
}

rc<-as.data.frame(rc)
colnames(rc) <- as.character(gsub("-","_",titles, fixed=TRUE))
rc$V1<-x$V1
buildDiffExpForAllContrasts(rc, 
                            list(
                              list(condition='Organoids_Amlodipine',control='Organoids_MOCK'),
                              list(condition='Organoids_Berbamine',control='Organoids_MOCK'),
                              list(condition='Organoids_DMSO',control='Organoids_MOCK'),
                              list(condition='Organoids_Loperamide',control='Organoids_MOCK'),
                              list(condition='Organoids_RS504393',control='Organoids_MOCK'),
                              list(condition='Organoids_Terfenadine',control='Organoids_MOCK'),
                              list(condition='Organoids_Trifluoperazine',control='Organoids_MOCK'),
                              
                              list(condition='ACE2_A549_Amlodipine_COV2',control='Mock'),
                              list(condition='ACE2_A549_Amlodipine_COV2',control='ACE2_A549_Amoldipine_drugonly'),
                              list(condition='ACE2_A549_Amoldipine_drugonly',control='Mock'),
                              
                              list(condition='ACE2_A549_Berbamine_COV2',control='Mock'),
                              list(condition='ACE2_A549_Berbamine_COV2',control='ACE2_A549_Berbamine_drugonly'),
                              list(condition='ACE2_A549_Berbamine_drugonly',control='Mock'),
                              
                              list(condition='ACE2_A549_DMSO_COV2',control='Mock'),
                               
                              list(condition='ACE2_A549_Loperamide_COV2',control='Mock'),
                              list(condition='ACE2_A549_Loperamide_COV2',control='ACE2_A549_Loperamide_drugonly'),
                              list(condition='ACE2_A549_Loperamide_drugonly',control='Mock'),
                              
                              list(condition='ACE2_A549_RS504393_COV2',control='Mock'),
                              list(condition='ACE2_A549_RS504393_COV2',control='ACE2_A549_RS504393_drugonly'),
                              list(condition='ACE2_A549_RS504393_drugonly',control='Mock'),
                              
                              list(condition='ACE2_A549_Terfenadine_COV2',control='Mock'),
                              list(condition='ACE2_A549_Terfenadine_COV2',control='ACE2_A549_Terfenadine_drugonly'),
                              list(condition='ACE2_A549_Terfenadine_drugonly',control='Mock'),
                              
                              list(condition='ACE2_A549_Trifluoperazine_COV2',control='Mock'),
                              list(condition='ACE2_A549_Trifluoperazine_COV2',control='ACE2_A549_Trifluoperazone_drugonly'),
                              list(condition='ACE2_A549_Trifluoperazone_drugonly',control='Mock')
                            ), "GSE154613")


# GSE155363 - 	Transcriptomic profiles of whole blood from SARS-CoV-2 rhesus macaques
require(GEOquery)
gse<-getGEO("GSE155363")

info<-list(
  acc=as.character(gse$GSE155363_series_matrix.txt.gz$geo_accession),
  agent=as.character(gse$GSE155363_series_matrix.txt.gz$characteristics_ch1),
  pheno=as.character(gse$GSE155363_series_matrix.txt.gz$characteristics_ch1.1),
  animal=as.character(gse$GSE155363_series_matrix.txt.gz$characteristics_ch1.2)
)

info_df <- data.frame(matrix(t(unlist(info)), nrow=length(info), byrow=T),stringsAsFactors=FALSE)

rc<-fread("https://ftp.ncbi.nlm.nih.gov/geo/series/GSE155nnn/GSE155363/suppl/GSE155363_COM_master_counts_refseq.txt.gz")
dim(rc)
sample_info<-fread("https://ftp.ncbi.nlm.nih.gov/geo/series/GSE155nnn/GSE155363/suppl/GSE155363_COM_master_targets.txt.gz", header = T)
colnames(rc) <- c('V1',gsub("(Monkey[5-8])","\\1ALL",sample_info$Monkey_Time_Condition, fixed=F))

buildDiffExpForAllContrasts(rc, 
                            list(
                              list(condition='ALL_D1_Case',control='ALL_D0_Control'),
                              list(condition='ALL_D3_Case',control='ALL_D0_Control'),
                              list(condition='ALL_D5_Case',control='ALL_D0_Control'),
                              list(condition='ALL_D7_Case',control='ALL_D0_Control'),
                              list(condition='ALL_D10_Case',control='ALL_D0_Control'),
                              list(condition='ALL_D12_Case',control='ALL_D0_Control'),
                              list(condition='ALL_D15_Case',control='ALL_D0_Control'),
                              list(condition='ALL_D17_Case',control='ALL_D0_Control')
                            ), "GSE155363")


colnames(rc) <- c('V1',sample_info$Monkey_Time_Condition)

buildDiffExpForAllContrasts(rc, 
                            list(
                              list(condition='D1_Case',control='D0_Control'),
                              list(condition='D3_Case',control='D0_Control'),
                              list(condition='D5_Case',control='D0_Control'),
                              list(condition='D7_Case',control='D0_Control'),
                              list(condition='D10_Case',control='D0_Control'),
                              list(condition='D12_Case',control='D0_Control'),
                              list(condition='D15_Case',control='D0_Control'),
                              list(condition='D17_Case',control='D0_Control')
                            ), "GSE155363")


rl<-data.frame()

for(d in c("D1","D3","D5","D7","D10","D12","D15","D17")){
  r<-fread(paste("GSE155363_ALL",d,"Case_VS_ALL_D0_Control.deseq2",sep="_"))
  rl[d,"DE"]<-sum(abs(r$log2FoldChange)>=0.585 & r$padj<.05)
  rl[d,"ALL"]<-dim(r)[1]
}

#     DE   ALL
# D1  1148 15702
# D3   842 25001
# D5  4419 32998
# D7   671 23022
# D10  564 22327
# D12 2526 15706
# D15   10  2383
# D17   18  3051


rl<-data.frame()

for(d in c("D1","D3","D5","D7","D10","D12","D15","D17")){
  r<-fread(paste("GSE155363",d,"Case_VS_D0_Control.deseq2",sep="_"))
  rl[d,"DE"]<-sum(abs(r$log2FoldChange)>=0.585 & r$padj<.05)
  rl[d,"ALL"]<-dim(r)[1]
}

#       DE   ALL
# D1    86 16380
# D3   778 25042
# D5  4231 32355
# D7   117 32355
# D10   11 34351
# D12 4142 23040
# D15    2 31033
# D17   17 21707

# sessionInfo()
# R version 3.6.3 (2020-02-29)
# Platform: x86_64-apple-darwin15.6.0 (64-bit)
# Running under: macOS Catalina 10.15.2
# 
# Matrix products: default
# BLAS:   /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libBLAS.dylib
# LAPACK: /Library/Frameworks/R.framework/Versions/3.6/Resources/lib/libRlapack.dylib
# 
# Random number generation:
#   RNG:     Mersenne-Twister 
# Normal:  Inversion 
# Sample:  Rounding 
# 
# locale:
#   [1] en_US.UTF-8/en_US.UTF-8/en_US.UTF-8/C/en_US.UTF-8/en_US.UTF-8
# 
# attached base packages:
#   [1] parallel  stats4    stats     graphics  grDevices utils     datasets  methods   base     
# 
# other attached packages:
#   [1] DESeq2_1.26.0               data.table_1.12.8           SummarizedExperiment_1.16.1
# [4] DelayedArray_0.12.3         BiocParallel_1.20.1         matrixStats_0.56.0         
# [7] Biobase_2.46.0              GenomicRanges_1.38.0        GenomeInfoDb_1.22.1        
# [10] IRanges_2.20.2              S4Vectors_0.24.4            BiocGenerics_0.32.0        
# 
# loaded via a namespace (and not attached):
#   [1] bit64_0.9-7            splines_3.6.3          R.utils_2.9.2          Formula_1.2-3         
# [5] assertthat_0.2.1       BiocManager_1.30.10    latticeExtra_0.6-29    blob_1.2.1            
# [9] GenomeInfoDbData_1.2.2 pillar_1.4.3           RSQLite_2.2.0          backports_1.1.6       
# [13] lattice_0.20-41        glue_1.4.0             digest_0.6.25          RColorBrewer_1.1-2    
# [17] XVector_0.26.0         checkmate_2.0.0        colorspace_1.4-1       htmltools_0.4.0       
# [21] Matrix_1.2-18          R.oo_1.23.0            XML_3.99-0.3           pkgconfig_2.0.3       
# [25] genefilter_1.68.0      zlibbioc_1.32.0        xtable_1.8-4           scales_1.1.0          
# [29] jpeg_0.1-8.1           htmlTable_1.13.3       tibble_3.0.0           annotate_1.64.0       
# [33] ggplot2_3.3.0          ellipsis_0.3.0         nnet_7.3-13            cli_2.0.2             
# [37] survival_3.1-12        magrittr_1.5           crayon_1.3.4           memoise_1.1.0         
# [41] R.methodsS3_1.8.0      fansi_0.4.1            foreign_0.8-76         tools_3.6.3           
# [45] lifecycle_0.2.0        stringr_1.4.0          locfit_1.5-9.4         munsell_0.5.0         
# [49] cluster_2.1.0          AnnotationDbi_1.48.0   compiler_3.6.3         rlang_0.4.5           
# [53] grid_3.6.3             RCurl_1.98-1.1         rstudioapi_0.11        htmlwidgets_1.5.1     
# [57] bitops_1.0-6           base64enc_0.1-3        gtable_0.3.0           curl_4.3              
# [61] DBI_1.1.0              R6_2.4.1               gridExtra_2.3          knitr_1.28            
# [65] bit_1.1-15.2           Hmisc_4.4-0            stringi_1.4.6          Rcpp_1.0.4            
# [69] geneplotter_1.64.0     vctrs_0.2.4            rpart_4.1-15           acepack_1.4.1         
# [73] png_0.1-7              xfun_0.12
