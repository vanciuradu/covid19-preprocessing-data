# README #


### What is this repository for? ###

* Collection of R scripts used to preprocess and analyze COVID-19 data into an input suitable for 
[iPathwayGuide](https://ipathwayguide.advaitabio.com)
* Version 0.0.1
* This is for research purpose only. This tool does not provide medical advice. It is intended for informational purposes only. It is not a substitute for professional medical advice, diagnosis or treatment. Never ignore professional medical advice in seeking treatment because of something you have read here.
* [GSE147507 data](https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSE147507&format=file&file=GSE147507%5FRawReadCounts%2Etsv%2Egz) was imported here for convenence. Use the latest data. All credits and proper acknowledgemets should be directed to Daniel Blanco Melo and the other contributors who make the data available.

####Selected datasets summary:

|Dataset|Tissue|Tissue Type|#Samples in each group|
|--|--|--|--|
|GSE147507|lung of deceased COVID-19 patient vs control |patient|2 vs 2|
|GSE147507|SARS-CoV-2 and other viruses vs control|cells|3 vs 3|
|GSE150316| lung of deceased COVID-19 patients with a high viral load|patient|29 vs 5|
|GSE160435|lung organoid|cells|5 vs 5|
|GSE149312|intestinal organoids|cells|3 vs 3|
|GSE153940|vero cells|cells|3 vs 3|

### How do I get set up? ###

Tested with R version 3.6.0 or 3.6.1 with Bioconductor 3.10 or 3.11

In your R terminal install dependencies:

```R
install.pakages("data.table")
BiocManager::install("DESeq2")
```

Set working directory to the the folder where this README.md file resides.

```
setwd("YOUR_FOLDER")
```

Run the R code.

```sh
RScript -e "setwd('YOUR_FOLDER');source('DESEQpipeline.R');"
```

 After running this command in your bash terminal, a set of 4 output files *.deseq2 are created with all the measured and differentially expressed genes in corresponding sample. The sample information in GEO was used to assign samples to groups. Feel free to load the R script in your favorite R enviroment and run it line by line. 

 *If you find issues please report back, via the issues tracker. Pull requests are welcomed.*

### Contribution guidelines ###

Add small (< 10 MB) files only. Preferably text files.

If you find issues in the R code please provide a pull request.

We are using the gitflow process: A stable master branch, a less stable develop branch and unstable feature branches.

If you want to contribute, create a feature branch and create a pull request against the develop branch.

### Who do I talk to? ###

* Repo owner or admin: Radu Vanciu: radu at advaitabio dot com
* Other community or team contact: [Advaita Bioinformatics](https://advaitabio.com)

### Released under MIT License

Copyright (c) 2020 Radu Vanciu
Copyright (c) 2020 Advaita Bioinformatics

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
